package com.star.robot.service;

import com.star.robot.dto.AdminIndexDto;
import com.star.robot.repository.CompanyRepository;
import com.star.robot.repository.TeamMemberRepository;
import com.star.robot.repository.TeamRepository;
import io.swagger.annotations.ApiOperation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.GetMapping;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExportTests {
    @Autowired
    private ExportService exportService;
    @Test
    public void exportTests(){
//        exportService.exportScore();
//        Integer lunci = null;
//        exportService.exportScore(lunci);
    }



    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    TeamMemberRepository teamMemberRepository;


   @Test
    public void indexData() {
//       String areaCode
        AdminIndexDto adminIndexDto = AdminIndexDto.builder()
                .cpmpanyCount(companyRepository.count())
                .schoolCount(companyRepository.countByType("0","41%"))
                .jigouCount(companyRepository.countByType("1","41%"))
                .teamCount(teamRepository.count())
                .memberCount(teamMemberRepository.count())
                .build();

       System.out.println(adminIndexDto.toString());

    }
}
