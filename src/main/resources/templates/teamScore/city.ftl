

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>成绩管理(市级)</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
  <link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
</head>
<body style = "overflow-y: scroll;">

  <div class="layui-fluid">
    <div class="layui-card">
		<div class="layui-form layui-card-header layuiadmin-card-header-auto">
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">单位名称：</label>
					<div class="layui-input-block">
						<input type="text" name="companyName" placeholder="请输入单位名称" autocomplete="off" class="layui-input" style="width:212px" id="companyName">
					</div>
				</div>
<#if admin.adminTypeEnum == "PROVINCEADMIN">

    <div class="layui-inline">
					<label class="layui-form-label">所在地区：</label>
					<div class="layui-input-block">
						<select name="provinceId" lay-filter="provinceId" class="provinceId" id="provinceId">
							<option value="">请选择所在省</option>
						</select>
					</div>
				</div>
				<div class="layui-inline">
					<select name="cityId" lay-filter="cityId" disabled id="cityId">
						<option value="">请选择所在市</option>
					</select>
				</div>
				<div class="layui-inline">
					<select name="areaId" lay-filter="areaId" disabled id="areaId">
						<option value="">请选择所在区</option>
					</select>
				</div>
</#if>
				<div class="layui-inline">
					<label class="layui-form-label">单位性质：</label>
					<div class="layui-inline">
						<input type="radio" name="companyType" value="" title="全部" checked>
						<input type="radio" name="companyType" value="1" title="学校" >
						<input type="radio" name="companyType" value="2" title="机构">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">参赛项目：</label>
					<div class="layui-input-block">
							<select name="projectId" lay-search id="projectId">
							<option value="">直接选择或搜索选择</option>
							<#list projectList as p>
								<option value="${p.id?c}">${p.name}</option>
							</#list>
						</select>
						
					</div>
					<#--<div class="layui-input-block">-->
					<#--<input type="text" name="projectName" placeholder="请输入参赛项目" autocomplete="off" class="layui-input" style="width:212px">-->
					<#--</div>-->
				</div>

				<div class="layui-inline">
					<label class="layui-form-label">组别：</label>
					<div class="layui-input-block">
						<select name="groupType" id="groupType">
							<option value="">请选择组别</option>
							<option value="1">小学</option>
							<option value="2">初中</option>
							<option value="3">高中</option>
						</select>
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">队伍名称：</label>
					<div class="layui-input-block">
						<input type="text" name="teamName" placeholder="请输入队伍名称" autocomplete="off" class="layui-input" style="width:212px" id="teamName">
					</div>
				</div>
				<div class="layui-outline">
					<#--<div class="layui-inline">-->
					<#--<label class="layui-form-label">队员姓名：</label>-->
					<#--<div class="layui-input-block">-->
					<#--<input type="text" name="teamMemberName" placeholder="请输入队员姓名" autocomplete="off" class="layui-input" style="width:212px">-->
					<#--</div>-->
					<#--</div>				-->

						<div class="layui-inline">
							<label class="layui-form-label">成绩总分：</label>
							<div class="layui-input-inline" style="width:20%">
								<input name="scoreStart" type="text" class="layui-input"  placeholder="请输入分数"  id="scoreStart"/>
							</div>
							<div class="layui-form-mid">分</div>
							<div class="layui-form-mid">
								-
							</div>
							<div class="layui-input-inline" style="width:20%">
								<input name="scoreEnd" type="text" class="layui-input"  placeholder="请输入分数" id="scoreEnd">
							</div>
							<div class="layui-form-mid">分</div>
						</div>
					<#--<div class="layui-inline">-->
					<#--<label class="layui-form-label">队员衣服尺码：</label>-->
					<#--<div class="layui-input-block">-->
					<#--<select name="clotherSizeEnum" >-->
					<#--<option value="">请选择尺码</option>-->
					<#--<option value="M">M</option>-->
					<#--<option value="L">L</option>-->
					<#--<option value="S">S</option>-->
					<#--<option value="XL">XL</option>-->
					<#--<option value="XXXL">XXXL</option>-->
					<#--</select>-->
					<#--</div>-->
					<#--</div>	-->
				</div>
				<div class="layui-outline">
					<#--<div class="layui-inline">-->
					<#--<label class="layui-form-label">队员身份证号：</label>-->
					<#--<div class="layui-input-block">-->
					<#--<input type="text" name="teamMemberIdCard" placeholder="请输入队员身份证号" autocomplete="off" class="layui-input" style="width:212px">-->
					<#--</div>-->
					<#--</div>-->
					<div class="layui-inline">
						<label class="layui-form-label">报名时间：</label>
						<div class="layui-input-inline">
							<input type="text" name="baoMingDateStart" class="layui-input" id="test-laydate-start" placeholder="报名开始时间">
						</div>
						<div class="layui-form-mid">
							-
						</div>
						<div class="layui-input-inline">
							<input type="text" name="baoMingDateEnd" class="layui-input" id="test-laydate-end" placeholder="报名结束时间">
						</div>
					</div>
					<div class="layui-inline">
						<button class="layui-btn layuiadmin-btn-list" lay-submit lay-filter="search">
							<i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
						</button>
					</div>
				</div>
			</div>

      <div class="layui-card-body">
        <div style="padding-bottom: 10px;">
          <#--<button class="layui-btn layuiadmin-btn-list" data-type="outExcle">导出excel</button>-->
			<#------------->
			<label class="layui-form-label" style="padding-left: 0px;">比赛轮次：</label>
			<div class="layui-input-inline" style="width:20%">
				<input  autocomplete="off" name="lunci" type="text" class="layui-input" placeholder="请输轮次,不填写默认是1" id="lunci">
				<#--<input type="text" name="lunci" placeholder="请输入单位名称" autocomplete="off" class="layui-input" style="width:212px" id="lunci">-->
			</div>
          <button class="layui-btn layuiadmin-btn-list" data-type="updateExcle">导出成绩模版</button>
          <button class="layui-btn layuiadmin-btn-list" id="test-upload-type1">导入成绩</button>
          <button class="layui-btn layui-btn-warm" data-type="publish" style="margin-left: 10px;">发布成绩</button>
          <#--<button class="layui-btn layui-btn-warm" data-type="add">批量省赛选拔报名</button>-->
        </div>
        <table id="manage" lay-filter="manage"></table> 
      </div>
    </div>
  </div>

  <script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>  
  <script>
  layui.config({
    base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
  }).extend({
    index: 'lib/index' //主入口模块
    ,address : 'address'
  }).use(['index', 'score', 'table', 'upload', 'jquery', 'address'], function(){
    var table = layui.table
    ,form = layui.form
    ,upload = layui.upload
    ,address = layui.address();
    
    //监听搜索
    form.on('submit(search)', function(data){
      var field = data.field;
      
      //执行重载
      table.reload('manage', {
        where: field
      });
    });
    
    var $ = layui.$, active = {
		publish : function(){
			location.href=  "/team/score/publish";
		},
      updateExcle: function(){
      		layer.open({
      		  icon: 1,
			  content: '导出量大的情况下需要较长时间，请耐心等待',
			  time: 7000,
			  closeBtn: 0,
			  success: function(layero, index){
			    var projectId = $('#projectId').val();
	      	   var companyName = $('#companyName').val();
			   var provinceId = $('#provinceId').val();
			   var cityId = $('#cityId').val();
			   var areaId = $('#areaId').val();
			   var companyType = $("input[name='companyType']:checked").val();		   
			   var groupType = $('#groupType').val();
			   var teamName = $('#teamName').val();
			   var scoreStart = $('#scoreStart').val();
			   var scoreEnd = $('#scoreEnd').val();
			   var baoMingDateStart = $('#test-laydate-start').val();
			   var baoMingDateEnd = $('#test-laydate-end').val();
			   var lunci = $('#lunci').val();
			   location.href="/downloadScoreFile?lunci="+lunci+"&companyName="+companyName+"&provinceId="+provinceId+"&cityId="+cityId+"&areaId="+areaId+"&companyType="+companyType+"&groupType="+groupType+"&teamName="+teamName+"&scoreStart="+scoreStart+"&scoreEnd="+scoreEnd+"&baoMingDateStart="+baoMingDateStart+"&baoMingDateEnd="+baoMingDateEnd+"&projectId="+projectId;
			  }
			});
		   

        // layer.open({
        //   type: 2
        //   ,title: '选择需要导出的字段'
        //   ,content: ['listformT', 'no']
        //   ,area: ['650px', '400px']
        //   ,btn: ['导出', '取消']
        //   ,yes: function(index, layero){
        //     //点击确认触发 iframe 内容中的按钮提交
        //     layer.close(index);
        //     location.href="/downloadScoreFile";
        //   }
        // });
      },
      outExcle: function(){

        layer.confirm('导出？', function(index){
          window.location.href="${request.contextPath}/js/机器人竞赛成绩表.xls";
          layer.close(index);
        });
      }
    }; 
    
    upload.render({
      elem: '#test-upload-type1'
      ,url: '/importScore'
      ,accept: 'file' //普通文件
      ,exts: 'xls' //xls
      ,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
	    layer.load(); //上传loading
	  }
      ,done: function(res, index, upload){
      	layer.closeAll('loading'); //关闭loading
      	layer.msg('上传成功', {
 			icon:1
      		,time: 1000
      		,shade: 0.3
      	});
      }
      ,error: function(index, upload){
	    layer.closeAll('loading'); //关闭loading
	    layer.msg('上传失败，请重新上传', {
 			icon:1
      		,time: 1000
      		,shade: 0.3
      	});
	  }
    });
    
    //监听行工具事件
	table.on('tool(manage)', function(obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
		var data = obj.data //获得当前行数据
			,
			layEvent = obj.event; //获得 lay-event 对应的值
		if(layEvent === 'result') {
			layer.open({
					type: 2,
					title: '查看成绩详情',
					content: ['resultform?teamId='+data.teamId],
					area: ['500px', '500px'],
					yes: function(index, layero) {
						//点击确认触发 iframe 内容中的按钮提交
						var submit = layero.find('iframe').contents().find("#layuiadmin-app-form-submit");
						submit.click();
					}
				});
		}
	});

    $('.layui-btn.layuiadmin-btn-list').on('click', function(){
      var type = $(this).data('type');
      active[type] ? active[type].call(this) : '';
    });

  }).use(['index', 'laydate'], function() {
		var laydate = layui.laydate;

		//开始日期
		var insStart = laydate.render({
			elem: '#test-laydate-start',
			type: 'datetime',
			done: function(value, date) {
				//更新结束日期的最小日期
				insEnd.config.min = lay.extend({}, date, {
					month: date.month - 1
				});
				//自动弹出结束日期的选择器
				insEnd.config.elem[0].focus();
			}
		});

		//结束日期
		var insEnd = laydate.render({
			elem: '#test-laydate-end',
			type: 'datetime'
		});
	});
  </script>
</body>
</html>
