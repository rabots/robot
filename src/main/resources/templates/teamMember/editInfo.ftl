<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>修改队员</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<link rel="stylesheet" href="${request.contextPath}/layuiadmin/layui/css/layui.css" media="all">
		<link rel="stylesheet" href="${request.contextPath}/layuiadmin/style/admin.css" media="all">
	</head>

	<body>
		<form class="layui-form">
			<div class="layui-fluid">
				<div class="layui-row layui-col-space15">
					<div class="layui-col-md12">
						<div class="layui-card">
							<div class="layui-card-header">修改队员</div>
							<div class="layui-form-item">
							</div>
							<div class="layui-form" lay-filter="">
								<div class="layui-form-item">
									<label class="layui-form-label">姓名：</label>
									<div class="layui-input-inline">
										<input type="text" name="name" value="${teamMember.name}" class="layui-input" style="width: 300px" placeholder="请输入营业执照名称">
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">性别：</label>
									<div class="layui-input-inline">
										<input type="radio" name="sex" value="1" title="男"  <#if teamMember.sex==1>checked</#if>>
										<input type="radio" name="sex" value="2" title="女"  <#if teamMember.sex==2>checked</#if>>
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">身份证号：</label>
									<div class="layui-input-inline">
										<input type="text" name="idCard" value="${teamMember.idCard}" class="layui-input" style="width: 300px" placeholder="请输入单位简称">
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">size：</label>
									<div class="layui-input-inline">
										<input type="text" name="size" value="${teamMember.size}" class="layui-input" style="width: 300px" placeholder="请输入单位简称">
									</div>
								</div>
								<div class="layui-form-item">
									<label class="layui-form-label">所属学校：</label>
									<div class="layui-input-inline">
										<input type="text" name="school" value="${teamMember.school}" class="layui-input" style="width: 300px" placeholder="请输入单位简称">
									</div>
								</div>
							</div>
							<div class="layui-form-item">
								<div class="layui-input-block" style="padding-bottom: 10px;">
									<button class="layui-btn" lay-submit lay-filter="addSubmit">提交</button>
									<input type="button" class="layui-btn layui-btn-primary" onclick="javascript:history.back(-1);" value="返回"></input>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

		<script src="${request.contextPath}/layuiadmin/layui/layui.js"></script>
		<script>
			layui.config({
					base: '${request.contextPath}/layuiadmin/' //静态资源所在路径
				}).extend({
					index: 'lib/index' //主入口模块
					,address : 'address'
				}).use(['index', 'set', 'form', 'jquery' ,'address'], function() {
					var form = layui.form
					,address = layui.address();
					var $ = layui.$;
					form.on('submit(addSubmit)', function(data) {
						$.ajax({
							type: 'post',
							url: '/team/member/modifyMemberBack',
							contentType: 'application/json;charset=utf-8',
							data: JSON.stringify(data.field),
							dataType: 'json',
							success: function(result, status, xhr) {
								layer.msg('修改成功', {icon: 1,offset: '200px',shade: 0.3,time: 1000}, function(){
					              location.href="teamManage";
					            });
								
							}
						});
						return false;
					});
				});
		</script>
	</body>

</html>