package com.star.robot.controller;

import com.star.robot.constant.Constant;
import com.star.robot.dto.TeamBackendQueryDto;
import com.star.robot.dto.TeamMemberQueryDto;
import com.star.robot.service.ExportService;
import com.star.robot.specialficaition.TeamSpecial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashSet;

@Controller
public class DownloadController {


    @Autowired
    private ExportService exportService;

    @Autowired
    private TeamSpecial teamSpecial;

    //文件下载相关代码
    @RequestMapping("/downloadScoreFile")
    public void downloadScoreFile(TeamBackendQueryDto requestDto ,  HttpServletRequest request, HttpServletResponse response,Integer lunci) {



        String fileName = exportService.exportScore(lunci , requestDto);
        downfile(response, fileName);
    }


    /**
     * 队员导出excel
     *
     * @param request
     * @param
     */
    @GetMapping("/downloadTeamMemberFile")
    public void downloadTeamMemberFile(HttpServletRequest request, HttpServletResponse response , TeamMemberQueryDto requestDto, String id) {

        System.out.println("--------------------" + id);

        String fileName = exportService.exportTeamMember(request, requestDto );
        downfile(response, fileName);
    }

    //文件下载相关代码
    @RequestMapping("/downloadTeamFile")
    public void downloadTeamFile(TeamBackendQueryDto requestDto,HttpServletRequest request, HttpServletResponse response) {
        String [] columnNames = getColumnName(requestDto);

        String fileName = exportService.exportTeam(requestDto);
        downfile(response, fileName);
    }

    private String[] getColumnName(TeamBackendQueryDto requestDto) {

        Boolean hasCondition = Boolean.FALSE;

        HashSet hashSet = new HashSet();

        //所在地区列导出
        if(!StringUtils.isEmpty(requestDto.getCompanyName())){
            hasCondition = Boolean.TRUE;
            hashSet.add("所在地区");
        }
        //单位性质列导出
        if(requestDto.getCompanyType() != null){
            hasCondition = Boolean.TRUE;
            hashSet.add("单位性质");
        }
        //单位名称列导出
        if(requestDto.getCompanyType() != null){
            hasCondition = Boolean.TRUE;
            hashSet.add("单位名称");
        }
        //队伍名称列导出
        if(requestDto.getCompanyType() != null){
            hasCondition = Boolean.TRUE;
            hashSet.add("队伍名称");
        }
        //组别
        if(requestDto.getGroupType() != null){
            hasCondition = Boolean.TRUE;
            hashSet.add("组别");
        }
        //组别
        if(requestDto.getProjectId() != null){
            hasCondition = Boolean.TRUE;
            hashSet.add("参赛项目");
        }

        hashSet.add("辅导老师1姓名");
        hashSet.add("辅导老师1电话");
        hashSet.add("辅导老师2姓名");
        hashSet.add("辅导老师2电话");


        if(false){
            String [] columnNames = (String [] )hashSet.toArray();
            return columnNames;
        }else{
            String [] columnNames = new String[]{ "所在地区" , "单位性质" ,"单位名称" ,  "队伍名称" , "参赛项目", "组别" , "辅导老师1姓名" , "辅导老师1电话" , "辅导老师2姓名" , "辅导老师2电话"};
            return columnNames;
        }

    }


    private void downfile(HttpServletResponse response, String fileName) {
        if (fileName != null) {
            //设置文件路径
            String realPath = Constant.downloadPath;
            File file = new File(realPath, fileName);
            if (file.exists()) {
                response.setContentType("application/octet-stream");//
                response.setHeader("content-type", "application/octet-stream");
                response.setHeader("Content-Disposition", "attachment;fileName=" + fileName);// 设置文件名
                byte[] buffer = new byte[1024];
                FileInputStream fis = null;
                BufferedInputStream bis = null;
                try {
                    fis = new FileInputStream(file);
                    bis = new BufferedInputStream(fis);
                    OutputStream os = response.getOutputStream();
                    int i = bis.read(buffer);
                    while (i != -1) {
                        os.write(buffer, 0, i);
                        i = bis.read(buffer);
                    }
                    System.out.println("success");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (bis != null) {
                        try {
                            bis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (fis != null) {
                        try {
                            fis.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

}
