package com.star.robot.controller;

import com.star.robot.dto.ResultDto;
import com.star.robot.dto.TeamMemberQueryDto;
import com.star.robot.entity.TeamLeader;
import com.star.robot.entity.TeamMember;
import com.star.robot.repository.TeamLeaderRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/team/leader")
public class TeamLeaderController {

    @Autowired
    private TeamLeaderRepository teamLeaderRepository;
    @GetMapping(value = "/teamId")
    @ApiOperation(value = "队伍辅导老师查询",notes = "队伍辅导老师查询")
    public ResultDto getTeamMember(TeamMemberQueryDto teamMemberQueryDto) {

        if(teamMemberQueryDto.getTeamId() != null){
            List<TeamLeader> teamLeaders =  teamLeaderRepository.getByTeamId(teamMemberQueryDto.getTeamId());

            if(!CollectionUtils.isEmpty(teamLeaders)){
                for(TeamLeader teamLeader : teamLeaders){
                    teamLeader.setTeam(null);
                }
            }
            return ResultDto.builder().data(teamLeaders).build();
        }
        return ResultDto.builder().data(null).build();

    }

}
