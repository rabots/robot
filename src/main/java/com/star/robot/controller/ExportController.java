package com.star.robot.controller;

import com.star.robot.dto.ExportScoreDto;
import com.star.robot.dto.ResultDto;
import com.star.robot.util.ExportExcel;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 导出管理 (队伍　，　队员　　，　成绩　，)
 */
@RestController(value = "/export")
public class ExportController {


    /**
     * 导出成绩
     * @return
     */
    public ResultDto exportScore(){


        return ResultDto.builder().build();
    }
}
