package com.star.robot.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.star.robot.dto.DtAreaRequestDto;
import com.star.robot.dto.ResultDto;
import com.star.robot.entity.DtArea;
import com.star.robot.repository.DtAreaRepositoty;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/dtArea")
public class DtAreaController<E> {
	
	@Autowired
    private DtAreaRepositoty dtAreaRepositoty;
	
	private static JSONArray jsonArray3 = new JSONArray();
	
	@PostConstruct
	private void initDtArea() {		
    	//省
    	List<DtArea> provinceList = (List<DtArea>)ResultDto.builder().data(dtAreaRepositoty.findAllByAreaParentId((long)0)).build().getData();
    	for (DtArea dtArea : provinceList) {
    		JSONArray jsonArray2 = new JSONArray();
    		JSONObject jsonObject3 = new JSONObject();
    		jsonObject3.put("code", dtArea.getId());
    		jsonObject3.put("name", dtArea.getAreaName());
    		//市
    		List<DtArea> cityList = (List<DtArea>)ResultDto.builder().data(dtAreaRepositoty.findAllByAreaParentId(dtArea.getId())).build().getData();
    		for (DtArea dtArea2 : cityList) {
    			JSONArray jsonArray1 = new JSONArray();
    			JSONObject jsonObject2 = new JSONObject();
    			jsonObject2.put("code", dtArea2.getId());
    	    	jsonObject2.put("name", dtArea2.getAreaName());
    	    	//区
    	    	List<DtArea> areaList = (List<DtArea>)ResultDto.builder().data(dtAreaRepositoty.findAllByAreaParentId(dtArea2.getId())).build().getData();
    	    	for (DtArea dtArea3 : areaList) {
    	    		JSONObject jsonObject1 = new JSONObject();
    	    		jsonObject1.put("code", dtArea3.getId());
        	    	jsonObject1.put("name", dtArea3.getAreaName());
        	    	jsonArray1.add(jsonObject1);
				}
    	    	jsonObject2.put("childs", jsonArray1);
    	    	jsonArray2.add(jsonObject2);
			}
    		jsonObject3.put("childs", jsonArray2);
    		jsonArray3.add(jsonObject3);
		}
	}

    

    /**
     * 查找全部省　省下市　市下区
     * @param requestDto
     * @return
     */
    @ApiOperation(value = "1.查找全部省,参数传0, 2.省下市 传对应省id, 3.市下区 传对应市id")
    @GetMapping(value = "/")
    public ResultDto findByParentId(DtAreaRequestDto requestDto){
        if(requestDto != null && requestDto.getParentAreaId()!= null){
            return ResultDto.builder().data(dtAreaRepositoty.findAllByAreaParentId(requestDto.getParentAreaId())).build();
        }else{
            return ResultDto.builder().build();
        }
    }
    
    /**
     * 查找全部省　省下市　市下区
     * @param requestDto
     * @return
     */
	@SuppressWarnings("unchecked")
	@GetMapping(value = "/getJsonDtArea")
    public JSONArray getJsonDtArea(DtAreaRequestDto requestDto){  	  	   	
        return jsonArray3;
    }


}
