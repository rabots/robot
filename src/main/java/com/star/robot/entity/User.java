package com.star.robot.entity;

import javax.persistence.*;

import com.star.robot.enums.AdminTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column(columnDefinition = "varchar(255) null COMMENT \"名称\"")
    private String account;

    @Column(columnDefinition = "varchar(255) null COMMENT \"真实姓名\"")
    private String realName;

    @Column(columnDefinition = "varchar(255) null COMMENT \"登录账号\"")
    private String phone ;

    @Column(columnDefinition = "varchar(255) null COMMENT \"登录密码\"")
    private String passwd;

    @Column
    private Integer provinceId;

    @Column
    private Integer cityId;

    @Column(columnDefinition = "int(10) null COMMENT \"所在城市\"")
    private Integer areaId;

    @OneToOne
    private Company company;

    @Column
    private Integer modifyPwd;
}
