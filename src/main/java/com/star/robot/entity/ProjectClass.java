package com.star.robot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.star.robot.enums.ProjectClassEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name = "project_class")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProjectClass {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer code;

    private String name;

    private ProjectClassEnum projectClassEnum;


    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "project_id",referencedColumnName = "id",columnDefinition = "int(10) null COMMENT \"项目id\"")
    private Project project;


}

