package com.star.robot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.star.robot.dto.PageRequestDto;
import com.star.robot.enums.CompanyTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * 单位管理，包含学校　和　机构
 */

@Entity
@Data
@NoArgsConstructor
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Company extends PageRequestDto {
    @Id
    @GeneratedValue
    private Long id;

    @Column(columnDefinition = "varchar(255) null COMMENT \"单位简称\"")
    private String name;

    @Column(columnDefinition = "varchar(255) null COMMENT \"营业执照名称\"")
    private String zhiZhao;

    @Column
    private Integer provinceId;

    @Column
    private Integer cityId;

    @Column(columnDefinition = "varchar(255) null COMMENT \"所在城市\"")
    private Integer areaId;
    
    private String location;

    @Column(columnDefinition = "varchar(255) null COMMENT \"XUEXIAO 学校　JIGOU　机构\"")
    private CompanyTypeEnum companyType;

    @Column(columnDefinition = "bit(1)  null COMMENT \"有效标志\"")
    private Boolean status;

    @Column(columnDefinition = "varchar(255) null COMMENT \"创建时间\"")
    private Date createTime;

    @Column(columnDefinition = "varchar(255) null COMMENT \"更新时间\"")
    private Date updateTime;

    @Column(columnDefinition = "varchar(255) null COMMENT \"发票抬头\"")
    private String invoiceHeader ;

    @Column(columnDefinition = "varchar(255) null COMMENT \"发票税号\"")
    private String invoiceTaxNo;

    @Column(columnDefinition = "varchar(255) null COMMENT \"联系人\"")
    private String contactPerson;

    @Column(columnDefinition = "varchar(255) null COMMENT \"联系电话\"")
    private String contactTel;

    @OneToMany(mappedBy = "company")
    @JsonIgnore
    private List<Team> teams;

    @OneToOne
    @JsonIgnore
    private User user;
}
