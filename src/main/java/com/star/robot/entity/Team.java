package com.star.robot.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.star.robot.dto.PageRequestDto;
import com.star.robot.enums.ProjectClassEnum;
/**
 * 项目
 */
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Team extends PageRequestDto {
    @Id
    @GeneratedValue
    private Long id;
//
//    private Long provinceId;
//
//    private Long cityId;
//
//    private Long areaId;

//    private Long class1Id; //第一大类

//    private Long class2Id; //第二小类

//    private TeamGroupTypeEnum groupType;
    
    private ProjectClassEnum projectClassEnum;

    private String teamName;//队伍名称

    @OneToOne
//    @JsonIgnore
    private Project project;

    @OneToMany(mappedBy = "team")
//    @JsonIgnore
    private List<TeamMember> teamMembers;

    @ManyToMany(mappedBy = "team")
//    @JsonIgnore
    private List<TeamLeader> teamLeaders;

//    private Integer special;//0 普通大类　1 特殊大类　是否显示三张图片
//
//    private String img1; //图片1
//
//    private String img2;//图片2
//
//    private String img3; //图片3

//    private Boolean word;//研究报告或者word文档
//
//    private String wordImg;//研究报告或者word文档预览图片

    private String phone; //关联用户

    private Date createTime;//创建时间

    private Boolean xuanBa;//是否选拔赛

    private Long templateId;//队伍模板id

    @OneToOne
//    @JsonIgnore
    private JiaoFei jiaoFei;

    @Column
    private Boolean confirmStatus;// 是否确认过报名

    @OneToMany(mappedBy = "team")
//    @JsonIgnore
    private List<TeamScore> teamScores;
    
    @Column
    private Boolean fapiaoStatus;//是否需要发票

    @ManyToOne
    @JoinColumn(name = "company_id",referencedColumnName = "id")
//    @JsonIgnore
    private Company company;

    @Column
    private  String status;

}
