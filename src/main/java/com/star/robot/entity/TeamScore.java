package com.star.robot.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 队伍成绩　
 */
@Entity
@Builder
@Table(name = "team_score")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeamScore {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Float score;//成绩

    @Column
    private Long lunCi;//轮次

    @Column
    private Boolean status;//fabu

    @ManyToOne
    @JoinColumn(name = "team_id",referencedColumnName = "id")
    private Team team;

}
