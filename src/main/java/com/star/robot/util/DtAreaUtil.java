package com.star.robot.util;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.star.robot.entity.DtArea;
import com.star.robot.repository.DtAreaRepositoty;

@Service
public class DtAreaUtil {
	@Autowired
    private DtAreaRepositoty dtAreaRepositoty;
	
	private Map<Long, Object> dtAreaMap = new ConcurrentHashMap<Long, Object>();
	
	@PostConstruct
	private void init() {
		List<DtArea> dtAreas = dtAreaRepositoty.findAll();
		for (DtArea dtArea : dtAreas) {
			dtAreaMap.put(dtArea.getId(), dtArea.getAreaName());
		}
	}
	
	public String getAreaName(Long id) {
		return (String) dtAreaMap.get(id);
	}

}
