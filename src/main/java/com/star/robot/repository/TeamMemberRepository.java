package com.star.robot.repository;

import com.star.robot.entity.TeamMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TeamMemberRepository extends JpaSpecificationExecutor, JpaRepository<TeamMember,Long> {
	public TeamMember findByIdCard(String idCard);

	@Query(value = "FROM  TeamMember WHERE TEAM_ID = ?1")
	public List<TeamMember> getByTeamId(Long teamId);

	@Query(nativeQuery = true , value = "select * FROM team_member where 1=1")
	public List<TeamMember> findAllSelf();

	@Query(value = "select count(*) FROM TeamMember where team_id =?1 ")
	public Long countTeamMember(Long teamId);

}
