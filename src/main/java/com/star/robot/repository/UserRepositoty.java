package com.star.robot.repository;

import com.star.robot.entity.DtArea;
import com.star.robot.entity.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepositoty extends CrudRepository<User, Long> , JpaSpecificationExecutor<User> {

    public User findByPhoneAndPasswd(String username , String passwd);

    public User findByPhone(String phone );

}
