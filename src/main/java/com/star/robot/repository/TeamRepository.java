package com.star.robot.repository;

import com.star.robot.entity.Company;
import com.star.robot.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TeamRepository extends JpaSpecificationExecutor, CrudRepository<Team,Long> {
    public Team getByPhone(String phone);
    public List<Team> getListByPhone(String phone);
    public List<Team> getListByCompany(Company company);
    public Team findByTeamNameAndCompany_Id(String teamName , Long companyId);
    public Team findLongByTemplateId(long templateId);
    public Team findLongById(long id);
    @Query(value ="SELECT COUNT(b.id) count FROM team a LEFT JOIN team_member b ON a.id=b.team_id WHERE a.id = ?1",nativeQuery = true)
    public int getMemberCount(String teamId);
}
