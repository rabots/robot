package com.star.robot.repository;

import com.star.robot.entity.JiaoFei;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface JiaoFeiRepository extends JpaSpecificationExecutor, JpaRepository<JiaoFei,Long> {
    JiaoFei findLongById(long id);
    @Transactional
    @Modifying
    @Query(value = "UPDATE jiao_fei a SET a.`status`=1 WHERE a.trade_no = ?1",nativeQuery = true)
    int updateStatus(String prepayId);
    @Transactional
    @Modifying
    @Query(value = "UPDATE jiao_fei a SET a.prepay_id=?1 WHERE a.trade_no=?2",nativeQuery = true)
    int savePrepayId(@Param("prepay_id") String prepay_id,@Param("trade_no") String trade_no);
}
