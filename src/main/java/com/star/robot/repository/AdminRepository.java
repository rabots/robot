package com.star.robot.repository;

import com.star.robot.entity.Admin;
import com.star.robot.entity.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface AdminRepository extends CrudRepository<Admin, Long>, JpaSpecificationExecutor<Admin> {
    public Admin findByUsernameAndPasswd(String username , String passwd);

    public Admin findByUsername(String username );

    public Admin findLongById(long id);
}
