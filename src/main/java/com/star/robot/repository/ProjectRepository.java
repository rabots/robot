package com.star.robot.repository;


import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.star.robot.entity.Project;
import com.star.robot.enums.ProjectLevelEnum;

public interface ProjectRepository extends CrudRepository<Project, Long> , JpaSpecificationExecutor<Project> {
    public Project findByName(String name);
    public Project findLongById(long id);
    public List<Project> findByProjectLevelEnum(ProjectLevelEnum projectLevelEnum);
    @Query(value = "select id,name from project where status =1 AND project_level_enum=0",nativeQuery=true)
    public List<Map<String,Object>> queryProject();
    public List<Project> findAll();



    @Query(value = "FROM  Project WHERE city_id = ?1")
    public List<Project> findByCityId(Long cityId);
}
