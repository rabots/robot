package com.star.robot.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.star.robot.entity.DtArea;

public interface DtAreaRepositoty extends CrudRepository<DtArea, Long> {

    public List<DtArea> findAllByAreaParentId(Long parentId);
    
    
    public DtArea findByid(Long id);
    
    public List<DtArea> findAll();
}
