package com.star.robot.specialficaition;

import com.star.robot.dto.PageResultDto;
import com.star.robot.dto.TeamMemberQueryDto;
import com.star.robot.dto.TeamMemberRespDto;
import com.star.robot.entity.DtArea;
import com.star.robot.entity.TeamMember;
import com.star.robot.enums.AdminTypeEnum;
import com.star.robot.enums.CompanyTypeEnum;
import com.star.robot.enums.ProjectClassEnum;
import com.star.robot.enums.ProjectLevelEnum;
import com.star.robot.repository.DtAreaRepositoty;
import com.star.robot.repository.TeamMemberRepository;
import com.star.robot.service.AdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class TeamMemberSpecial {

    @Autowired
    private TeamMemberRepository teamMemberRepository;

    @Autowired
    private AdminService adminService;

    @Autowired
    private DtAreaRepositoty dtAreaRepositoty;

    public List<TeamMember> findByRequest(TeamMemberQueryDto requestDto,HttpServletRequest request){
        PageResultDto<TeamMemberRespDto> results = new PageResultDto<>();

        Pageable page = new PageRequest(0, Integer.MAX_VALUE);

        Page<TeamMember> pageable = teamMemberRepository.findAll(new Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery cq, CriteriaBuilder cb) {
                List<Predicate> conditions = new ArrayList<>();

                //项目id
                if(requestDto.getProjectId() != null){
                    Predicate projectCond = cb.equal(root.get("team").get("project").get("id") , requestDto.getProjectId());
                    conditions.add(projectCond);
                }


                //项目名称
//                    if(!StringUtils.isEmpty(requestDto.getProjectName())){
//                        Predicate cityCondition = cb.equal(root.get("team").get("project").get("name")  , "%"+requestDto.getProjectName()+"%");
//                        conditions.add(cityCondition);
//                    }
                AdminTypeEnum adminTypeEnum = adminService.getCurrentAdminLevel(request);
                log.info("队员管理查询　：　参数[adminTypeEnum] :"+adminTypeEnum.getName());
                if(adminTypeEnum == AdminTypeEnum.PROVINCEADMIN){
                    Predicate adminTypeCond = cb.equal(root.get("team").get("project").get("projectLevelEnum") , ProjectLevelEnum.PROVINCELEVEL);
                    conditions.add(adminTypeCond);
                }else{
                    Predicate adminTypeCond = cb.equal(root.get("team").get("project").get("projectLevelEnum") , ProjectLevelEnum.PROVINCELEVEL);
                    conditions.add(adminTypeCond);

                    //市
                    Long cityId = adminService.getCurrentAdminCityId(request);
                    log.info("队员管理查询　：　参数[cityId] :"+cityId);
                    if(cityId != null){
                        Predicate cityCondition = cb.equal(root.get("team").get("company").get("cityId")  , cityId);
                        conditions.add(cityCondition);
                    }
                }

                //区
//                    if(requestDto.getAreaId() != null){
//                        Predicate areaCondition = cb.equal(root.get("team").get("company").get("areaId")  , requestDto.getAreaId());
//                        conditions.add(areaCondition);
//                    }
                //队员姓名
                if(!StringUtils.isEmpty(requestDto.getTeamMemberName())){
                    Predicate memberNameCond = cb.like(root.get("name")  , "%"+requestDto.getTeamMemberName()+"%");
                    conditions.add(memberNameCond);
                }
                //队员身份证
                if(!StringUtils.isEmpty(requestDto.getTeamMemberIdCard())){
                    Predicate memberIdCardCond = cb.like(root.get("idCard")  , "%"+requestDto.getTeamMemberIdCard()+"%");
                    conditions.add(memberIdCardCond);
                }
                //单位名称
                if(!StringUtils.isEmpty(requestDto.getCompanyName())){
                    Predicate companyCond = cb.like(root.get("team").get("company").get("name") , "%"+requestDto.getCompanyName() +"%");
                    conditions.add(companyCond);
                }
                //队伍名称
                if(!StringUtils.isEmpty(requestDto.getTeamName())){
                    Predicate teamNameCond = cb.like(root.get("team").get("teamName") , "%"+requestDto.getTeamName()+"%");
                    conditions.add(teamNameCond);
                }
                //单位性质
                if(requestDto.getCompanyType() !=null){
                    CompanyTypeEnum companyType = null;
                    if(requestDto.getCompanyType().intValue() == 1){
                        companyType =  CompanyTypeEnum.XUEXIAO;
                    }else if(requestDto.getCompanyType().intValue() == 2){
                        companyType =  CompanyTypeEnum.JIGOU;
                    }else{
                        //默认学校
                        companyType =  CompanyTypeEnum.XUEXIAO;
                    }
                    Predicate companyTypeCond = cb.equal(root.get("team").get("company").get("companyType") ,companyType);
                    conditions.add(companyTypeCond);

                }

                //报名时间 => 队伍　新增时间
                if(!StringUtils.isEmpty(requestDto.getBaoMingDateStart())
                ){
                    Predicate baoMingDateCond = cb.greaterThan(root.get("team").get("project").get("baoMingStartTime") ,requestDto.getBaoMingDateStart());
                    conditions.add(baoMingDateCond);
                }
                if(!StringUtils.isEmpty(requestDto.getBaoMingDateEnd())
                ){
                    Predicate baoMingDateCond = cb.lessThan(root.get("team").get("project").get("baoMingEndTime") , requestDto.getBaoMingDateEnd());
                    conditions.add(baoMingDateCond);
                }
                //参赛类别
                //大类
//                    if(requestDto.getClass1Id() != null){
//                        Predicate class1IdCond = cb.equal(root.get("team").get("class1Id") ,requestDto.getClass1Id());
//                        conditions.add(class1IdCond);
//                    }
//                    //小类
//                    if(requestDto.getClass2Id() != null){
//                        Predicate class2IdCond = cb.equal(root.get("team").get("class2Id") ,requestDto.getClass2Id());
//                        conditions.add(class2IdCond);
//                    }
                //组别
                if(requestDto.getGroupType() != null){
                    ProjectClassEnum projectClassEnum = null;
                    if(requestDto.getGroupType() == 1){
                        projectClassEnum = ProjectClassEnum.XIAOXUE;
                    }else if(requestDto.getGroupType() == 2){
                        projectClassEnum = ProjectClassEnum.CHUZHONG;
                    }else if(requestDto.getGroupType() == 3){
                        projectClassEnum = ProjectClassEnum.GAOZHONG;
                    }else{
                        //默认小学组
                        projectClassEnum = ProjectClassEnum.XIAOXUE;
                    }
                    Predicate groupTypeCond = cb.equal(root.get("team").get("projectClassEnum") ,projectClassEnum);
                    conditions.add(groupTypeCond);
                }
                if(requestDto.getSizeEnum() != null){
                    Predicate sizeCond = cb.equal(root.get("size") ,requestDto.getSizeEnum());
                    conditions.add(sizeCond);
                }

                Predicate[] pre = new Predicate[conditions.size()];

                cq.where(conditions.toArray(pre));
                return cq.getRestriction();
            }
        } , page);





        return pageable.getContent();
    }
}
