package com.star.robot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TeamDownloadDto {
    private String areaName;

    private String companyType;

    private String companyName;

    private String teamName;

    private String projectName;

    private String teamType;

    private String leader1Name;

    private String leader1Phone;

    private String leader2Name;

    private String leader2Phone;
}
