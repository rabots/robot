package com.star.robot.dto;
import com.star.robot.enums.TeamClotherSizeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TeamMemberRespDto {

    String companyType ;
    String companyName ;
    String teamName ;
    String projectType ;
    String projectId ;
    String groupType ;

    private String name;

    private String sex;

    private String idCard; //身份证

    private String size;

    private String school;//所在学校

    private String areaName;

    private String cityName;

    private Long id;
}
