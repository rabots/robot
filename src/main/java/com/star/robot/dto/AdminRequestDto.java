package com.star.robot.dto;

import com.star.robot.entity.Admin;
import com.star.robot.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AdminRequestDto extends Admin {
    private Long id;//id

    private String verfiCode;//验证码

    private String newPass;//新密码
}
