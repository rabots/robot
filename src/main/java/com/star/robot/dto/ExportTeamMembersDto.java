package com.star.robot.dto;
import com.star.robot.entity.Team;
import com.star.robot.enums.TeamClotherSizeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExportTeamMembersDto {
//    "队员姓名" , "单位类型" , "单位名称" , "队伍名称" , "参赛类别" , "组别","性别","身份证号","队服尺码","所属学校"};

//    private Long id;

    private String name;
    private String companyType;
    private String companyName;
    private String teamName;
    private String projectName;

    private String projectClass;
    private String sex;
    private String idCard; //身份证
    private String size;
    private String school;









//    private String zubie;




}
