package com.star.robot.enums;


public enum AdminTypeEnum {
    PROVINCEADMIN(0,"省级管理员") , CITYADMIN(1,"市级管理员");
    private Integer code;

    private String name;

    AdminTypeEnum(Integer code ,String name){
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
