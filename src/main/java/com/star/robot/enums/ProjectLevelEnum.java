package com.star.robot.enums;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public enum ProjectLevelEnum {
    PROVINCELEVEL(0,"省级别") , CITYLEVEL(1,"市级别");
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer code;


    private String name;



    ProjectLevelEnum(Integer code,String name){
        this.code= code;
        this.name = name;
    }

}

