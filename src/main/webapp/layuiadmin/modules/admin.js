/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
    function(e) {
        var t = layui.$,
            i = layui.table;
        layui.form;
        i.render({
            elem: "#adminTable",
            url: "/admin",
            cols: [[{
                type: "checkbox",
                fixed: "left"
            },
                {
                    field: "zz",
                    title: "序号",
                    type:"numbers"
                },
                {
                    field: "id",
                    title: "所属地区"
                },
                {
                    field: "id_card",
                    title: "用户名"
                },
                {
                    field: "name",
                    title: "密码"
                },
                {
                    field: "school",
                    title: "有效标志"
                },
                {
                    field: "sex",
                    title: "权限"
                },
                {
                    field: "修改",
                    title: "操作",
                    templet: function(res){
                        return '<a style="color:blue;cursor:pointer" href="" lay-event="detail">修改</a> ';
                    }
                }]],
                text: {none: '一条数据也没有^_^'}
        }),
            e("teammanagement", {})
    });