/** layuiAdmin.std-v1.1.0 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(e) {
    var t = layui.$,
    i = layui.table;
    layui.form;   
    i.render({
        elem: "#manage",
        url: "jiaofei/applyEd",
        page: true,
        cols: [[{
            type: "checkbox",
            fixed: "left"
        },
        {
            field: "zz",
            title: "序号",
            type:"numbers"
        },
        {
            field: "companyName",
            title: "单位名称"
        },
        {
            field: "areaName",
            title: "所在地区"
        },
        {
            field: "companyType",
            title: "单位性质",
            templet: function (d) {
                if(d.companyType == '0'){
                    return "学校";
                }
                if(d.companyType == '1'){
                    return "机构";
                }
            }
        },
        {
            field: "teamName",
            title: "队名"
        },
            {
                field: "projectName",
                title: "项目名称"
            },
            {
                field: "count",
                title: "参赛队员总数"
            },
        {
            field: "createTime",
            title: "创建时间",
            templet: function (d) {
                return  layui.util.toDateString(new Date(d.createTime));
            }
        }/*,
        {
            field: "修改",
            title: "操作"
        }*/]],
        text: {none: '一条数据也没有^_^'}
    }),
    e("signupinfo", {})
});