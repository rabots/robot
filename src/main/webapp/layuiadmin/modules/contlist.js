/** layuiAdmin.std-v1.2.1 LPPL License By http://www.layui.com/admin/ */
;
layui.define(["table", "form"],
function(t) {
    var e = layui.$,
    i = layui.table,
    n = layui.form;
    i.render({
        elem: "#LAY-app-content-list",
        url: layui.setter.base + "json/content/list.js",
        cols: [[{
            type: "checkbox",
            fixed: "left"
        },
        {
            field: "id",
            width: 100,
            title: "文章ID",
            sort: !0
        },
        {
            field: "label",
            title: "文章标签",
            minWidth: 100
        },
        {
            field: "title",
            title: "文章标题"
        },
        {
            field: "author",
            title: "作者"
        },
        {
            field: "uploadtime",
            title: "上传时间",
            sort: !0
        },
        {
            field: "status",
            title: "发布状态",
            templet: "#buttonTpl",
            minWidth: 80,
            align: "center"
        },
        {
            title: "操作",
            minWidth: 150,
            align: "center",
            fixed: "right",
            toolbar: "#table-content-list"
        }]],
        page: !0,
        limit: 10,
        limits: [10, 15, 20, 25, 30],
        text: "对不起，加载出现异常！"
    }),
    t("contlist", {})
});