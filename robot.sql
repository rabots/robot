# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17-log)
# Database: robot
# Generation Time: 2019-04-05 14:58:29 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table company
# ------------------------------------------------------------

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` bigint(20) NOT NULL,
  `area_id` varchar(255) DEFAULT NULL,
  `city_id` varchar(255) DEFAULT NULL,
  `company_type_enum` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `province_id` varchar(255) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `zhi_zhao` varchar(255) DEFAULT NULL,
  `company_type` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `invoice_header` varchar(255) DEFAULT NULL,
  `invoice_tax_no` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table dt_area
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dt_area`;

CREATE TABLE `dt_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '区域主键',
  `area_name` varchar(16) DEFAULT NULL COMMENT '区域名称',
  `area_code` varchar(128) DEFAULT NULL COMMENT '区域代码',
  `area_parent_id` int(11) DEFAULT NULL COMMENT '上级主键',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`area_parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=900001 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='区域字典';

LOCK TABLES `dt_area` WRITE;
/*!40000 ALTER TABLE `dt_area` DISABLE KEYS */;

INSERT INTO `dt_area` VALUES ('410000', '河南省', 'Henan', '0');
INSERT INTO `dt_area` VALUES ('410100', '郑州市', 'Zhengzhou', '410000');
INSERT INTO `dt_area` VALUES ('410102', '中原区', 'Zhongyuan', '410100');
INSERT INTO `dt_area` VALUES ('410103', '二七区', 'Erqi', '410100');
INSERT INTO `dt_area` VALUES ('410104', '管城回族区', 'Guancheng', '410100');
INSERT INTO `dt_area` VALUES ('410105', '金水区', 'Jinshui', '410100');
INSERT INTO `dt_area` VALUES ('410106', '上街区', 'Shangjie', '410100');
INSERT INTO `dt_area` VALUES ('410108', '惠济区', 'Huiji', '410100');
INSERT INTO `dt_area` VALUES ('410122', '中牟县', 'Zhongmu', '410100');
INSERT INTO `dt_area` VALUES ('410181', '巩义市', 'Gongyi', '410100');
INSERT INTO `dt_area` VALUES ('410182', '荥阳市', 'Xingyang', '410100');
INSERT INTO `dt_area` VALUES ('410183', '新密市', 'Xinmi', '410100');
INSERT INTO `dt_area` VALUES ('410184', '新郑市', 'Xinzheng', '410100');
INSERT INTO `dt_area` VALUES ('410185', '登封市', 'Dengfeng', '410100');
INSERT INTO `dt_area` VALUES ('410200', '开封市', 'Kaifeng', '410000');
INSERT INTO `dt_area` VALUES ('410202', '龙亭区', 'Longting', '410200');
INSERT INTO `dt_area` VALUES ('410203', '顺河回族区', 'Shunhe', '410200');
INSERT INTO `dt_area` VALUES ('410204', '鼓楼区', 'Gulou', '410200');
INSERT INTO `dt_area` VALUES ('410205', '禹王台区', 'Yuwangtai', '410200');
INSERT INTO `dt_area` VALUES ('410212', '祥符区', 'Xiangfu', '410200');
INSERT INTO `dt_area` VALUES ('410221', '杞县', 'Qixian', '410200');
INSERT INTO `dt_area` VALUES ('410222', '通许县', 'Tongxu', '410200');
INSERT INTO `dt_area` VALUES ('410223', '尉氏县', 'Weishi', '410200');
INSERT INTO `dt_area` VALUES ('410225', '兰考县', 'Lankao', '410200');
INSERT INTO `dt_area` VALUES ('410300', '洛阳市', 'Luoyang', '410000');
INSERT INTO `dt_area` VALUES ('410302', '老城区', 'Laocheng', '410300');
INSERT INTO `dt_area` VALUES ('410303', '西工区', 'Xigong', '410300');
INSERT INTO `dt_area` VALUES ('410304', '瀍河回族区', 'Chanhe', '410300');
INSERT INTO `dt_area` VALUES ('410305', '涧西区', 'Jianxi', '410300');
INSERT INTO `dt_area` VALUES ('410306', '吉利区', 'Jili', '410300');
INSERT INTO `dt_area` VALUES ('410311', '洛龙区', 'Luolong', '410300');
INSERT INTO `dt_area` VALUES ('410322', '孟津县', 'Mengjin', '410300');
INSERT INTO `dt_area` VALUES ('410323', '新安县', 'Xinan', '410300');
INSERT INTO `dt_area` VALUES ('410324', '栾川县', 'Luanchuan', '410300');
INSERT INTO `dt_area` VALUES ('410325', '嵩县', 'Songxian', '410300');
INSERT INTO `dt_area` VALUES ('410326', '汝阳县', 'Ruyang', '410300');
INSERT INTO `dt_area` VALUES ('410327', '宜阳县', 'Yiyang', '410300');
INSERT INTO `dt_area` VALUES ('410328', '洛宁县', 'Luoning', '410300');
INSERT INTO `dt_area` VALUES ('410329', '伊川县', 'Yichuan', '410300');
INSERT INTO `dt_area` VALUES ('410381', '偃师市', 'Yanshi', '410300');
INSERT INTO `dt_area` VALUES ('410400', '平顶山市', 'Pingdingshan', '410000');
INSERT INTO `dt_area` VALUES ('410402', '新华区', 'Xinhua', '410400');
INSERT INTO `dt_area` VALUES ('410403', '卫东区', 'Weidong', '410400');
INSERT INTO `dt_area` VALUES ('410404', '石龙区', 'Shilong', '410400');
INSERT INTO `dt_area` VALUES ('410411', '湛河区', 'Zhanhe', '410400');
INSERT INTO `dt_area` VALUES ('410421', '宝丰县', 'Baofeng', '410400');
INSERT INTO `dt_area` VALUES ('410422', '叶县', 'Yexian', '410400');
INSERT INTO `dt_area` VALUES ('410423', '鲁山县', 'Lushan', '410400');
INSERT INTO `dt_area` VALUES ('410425', '郏县', 'Jiaxian', '410400');
INSERT INTO `dt_area` VALUES ('410481', '舞钢市', 'Wugang', '410400');
INSERT INTO `dt_area` VALUES ('410482', '汝州市', 'Ruzhou', '410400');
INSERT INTO `dt_area` VALUES ('410500', '安阳市', 'Anyang', '410000');
INSERT INTO `dt_area` VALUES ('410502', '文峰区', 'Wenfeng', '410500');
INSERT INTO `dt_area` VALUES ('410503', '北关区', 'Beiguan', '410500');
INSERT INTO `dt_area` VALUES ('410505', '殷都区', 'Yindu', '410500');
INSERT INTO `dt_area` VALUES ('410506', '龙安区', 'Longan', '410500');
INSERT INTO `dt_area` VALUES ('410522', '安阳县', 'Anyang', '410500');
INSERT INTO `dt_area` VALUES ('410523', '汤阴县', 'Tangyin', '410500');
INSERT INTO `dt_area` VALUES ('410526', '滑县', 'Huaxian', '410500');
INSERT INTO `dt_area` VALUES ('410527', '内黄县', 'Neihuang', '410500');
INSERT INTO `dt_area` VALUES ('410581', '林州市', 'Linzhou', '410500');
INSERT INTO `dt_area` VALUES ('410600', '鹤壁市', 'Hebi', '410000');
INSERT INTO `dt_area` VALUES ('410602', '鹤山区', 'Heshan', '410600');
INSERT INTO `dt_area` VALUES ('410603', '山城区', 'Shancheng', '410600');
INSERT INTO `dt_area` VALUES ('410611', '淇滨区', 'Qibin', '410600');
INSERT INTO `dt_area` VALUES ('410621', '浚县', 'Xunxian', '410600');
INSERT INTO `dt_area` VALUES ('410622', '淇县', 'Qixian', '410600');
INSERT INTO `dt_area` VALUES ('410700', '新乡市', 'Xinxiang', '410000');
INSERT INTO `dt_area` VALUES ('410702', '红旗区', 'Hongqi', '410700');
INSERT INTO `dt_area` VALUES ('410703', '卫滨区', 'Weibin', '410700');
INSERT INTO `dt_area` VALUES ('410704', '凤泉区', 'Fengquan', '410700');
INSERT INTO `dt_area` VALUES ('410711', '牧野区', 'Muye', '410700');
INSERT INTO `dt_area` VALUES ('410721', '新乡县', 'Xinxiang', '410700');
INSERT INTO `dt_area` VALUES ('410724', '获嘉县', 'Huojia', '410700');
INSERT INTO `dt_area` VALUES ('410725', '原阳县', 'Yuanyang', '410700');
INSERT INTO `dt_area` VALUES ('410726', '延津县', 'Yanjin', '410700');
INSERT INTO `dt_area` VALUES ('410727', '封丘县', 'Fengqiu', '410700');
INSERT INTO `dt_area` VALUES ('410728', '长垣县', 'Changyuan', '410700');
INSERT INTO `dt_area` VALUES ('410781', '卫辉市', 'Weihui', '410700');
INSERT INTO `dt_area` VALUES ('410782', '辉县市', 'Huixian', '410700');
INSERT INTO `dt_area` VALUES ('410800', '焦作市', 'Jiaozuo', '410000');
INSERT INTO `dt_area` VALUES ('410802', '解放区', 'Jiefang', '410800');
INSERT INTO `dt_area` VALUES ('410803', '中站区', 'Zhongzhan', '410800');
INSERT INTO `dt_area` VALUES ('410804', '马村区', 'Macun', '410800');
INSERT INTO `dt_area` VALUES ('410811', '山阳区', 'Shanyang', '410800');
INSERT INTO `dt_area` VALUES ('410821', '修武县', 'Xiuwu', '410800');
INSERT INTO `dt_area` VALUES ('410822', '博爱县', 'Boai', '410800');
INSERT INTO `dt_area` VALUES ('410823', '武陟县', 'Wuzhi', '410800');
INSERT INTO `dt_area` VALUES ('410825', '温县', 'Wenxian', '410800');
INSERT INTO `dt_area` VALUES ('410882', '沁阳市', 'Qinyang', '410800');
INSERT INTO `dt_area` VALUES ('410883', '孟州市', 'Mengzhou', '410800');
INSERT INTO `dt_area` VALUES ('410900', '濮阳市', 'Puyang', '410000');
INSERT INTO `dt_area` VALUES ('410902', '华龙区', 'Hualong', '410900');
INSERT INTO `dt_area` VALUES ('410922', '清丰县', 'Qingfeng', '410900');
INSERT INTO `dt_area` VALUES ('410923', '南乐县', 'Nanle', '410900');
INSERT INTO `dt_area` VALUES ('410926', '范县', 'Fanxian', '410900');
INSERT INTO `dt_area` VALUES ('410927', '台前县', 'Taiqian', '410900');
INSERT INTO `dt_area` VALUES ('410928', '濮阳县', 'Puyang', '410900');
INSERT INTO `dt_area` VALUES ('411000', '许昌市', 'Xuchang', '410000');
INSERT INTO `dt_area` VALUES ('411002', '魏都区', 'Weidu', '411000');
INSERT INTO `dt_area` VALUES ('411023', '许昌县', 'Xuchang', '411000');
INSERT INTO `dt_area` VALUES ('411024', '鄢陵县', 'Yanling', '411000');
INSERT INTO `dt_area` VALUES ('411025', '襄城县', 'Xiangcheng', '411000');
INSERT INTO `dt_area` VALUES ('411081', '禹州市', 'Yuzhou', '411000');
INSERT INTO `dt_area` VALUES ('411082', '长葛市', 'Changge', '411000');
INSERT INTO `dt_area` VALUES ('411100', '漯河市', 'Luohe', '410000');
INSERT INTO `dt_area` VALUES ('411102', '源汇区', 'Yuanhui', '411100');
INSERT INTO `dt_area` VALUES ('411103', '郾城区', 'Yancheng', '411100');
INSERT INTO `dt_area` VALUES ('411104', '召陵区', 'Zhaoling', '411100');
INSERT INTO `dt_area` VALUES ('411121', '舞阳县', 'Wuyang', '411100');
INSERT INTO `dt_area` VALUES ('411122', '临颍县', 'Linying', '411100');
INSERT INTO `dt_area` VALUES ('411200', '三门峡市', 'Sanmenxia', '410000');
INSERT INTO `dt_area` VALUES ('411202', '湖滨区', 'Hubin', '411200');
INSERT INTO `dt_area` VALUES ('411221', '渑池县', 'Mianchi', '411200');
INSERT INTO `dt_area` VALUES ('411222', '陕县', 'Shanxian', '411200');
INSERT INTO `dt_area` VALUES ('411224', '卢氏县', 'Lushi', '411200');
INSERT INTO `dt_area` VALUES ('411281', '义马市', 'Yima', '411200');
INSERT INTO `dt_area` VALUES ('411282', '灵宝市', 'Lingbao', '411200');
INSERT INTO `dt_area` VALUES ('411300', '南阳市', 'Nanyang', '410000');
INSERT INTO `dt_area` VALUES ('411302', '宛城区', 'Wancheng', '411300');
INSERT INTO `dt_area` VALUES ('411303', '卧龙区', 'Wolong', '411300');
INSERT INTO `dt_area` VALUES ('411321', '南召县', 'Nanzhao', '411300');
INSERT INTO `dt_area` VALUES ('411322', '方城县', 'Fangcheng', '411300');
INSERT INTO `dt_area` VALUES ('411323', '西峡县', 'Xixia', '411300');
INSERT INTO `dt_area` VALUES ('411324', '镇平县', 'Zhenping', '411300');
INSERT INTO `dt_area` VALUES ('411325', '内乡县', 'Neixiang', '411300');
INSERT INTO `dt_area` VALUES ('411326', '淅川县', 'Xichuan', '411300');
INSERT INTO `dt_area` VALUES ('411327', '社旗县', 'Sheqi', '411300');
INSERT INTO `dt_area` VALUES ('411328', '唐河县', 'Tanghe', '411300');
INSERT INTO `dt_area` VALUES ('411329', '新野县', 'Xinye', '411300');
INSERT INTO `dt_area` VALUES ('411330', '桐柏县', 'Tongbai', '411300');
INSERT INTO `dt_area` VALUES ('411381', '邓州市', 'Dengzhou', '411300');
INSERT INTO `dt_area` VALUES ('411400', '商丘市', 'Shangqiu', '410000');
INSERT INTO `dt_area` VALUES ('411402', '梁园区', 'Liangyuan', '411400');
INSERT INTO `dt_area` VALUES ('411403', '睢阳区', 'Suiyang', '411400');
INSERT INTO `dt_area` VALUES ('411421', '民权县', 'Minquan', '411400');
INSERT INTO `dt_area` VALUES ('411422', '睢县', 'Suixian', '411400');
INSERT INTO `dt_area` VALUES ('411423', '宁陵县', 'Ningling', '411400');
INSERT INTO `dt_area` VALUES ('411424', '柘城县', 'Zhecheng', '411400');
INSERT INTO `dt_area` VALUES ('411425', '虞城县', 'Yucheng', '411400');
INSERT INTO `dt_area` VALUES ('411426', '夏邑县', 'Xiayi', '411400');
INSERT INTO `dt_area` VALUES ('411481', '永城市', 'Yongcheng', '411400');
INSERT INTO `dt_area` VALUES ('411500', '信阳市', 'Xinyang', '410000');
INSERT INTO `dt_area` VALUES ('411502', '浉河区', 'Shihe', '411500');
INSERT INTO `dt_area` VALUES ('411503', '平桥区', 'Pingqiao', '411500');
INSERT INTO `dt_area` VALUES ('411521', '罗山县', 'Luoshan', '411500');
INSERT INTO `dt_area` VALUES ('411522', '光山县', 'Guangshan', '411500');
INSERT INTO `dt_area` VALUES ('411523', '新县', 'Xinxian', '411500');
INSERT INTO `dt_area` VALUES ('411524', '商城县', 'Shangcheng', '411500');
INSERT INTO `dt_area` VALUES ('411525', '固始县', 'Gushi', '411500');
INSERT INTO `dt_area` VALUES ('411526', '潢川县', 'Huangchuan', '411500');
INSERT INTO `dt_area` VALUES ('411527', '淮滨县', 'Huaibin', '411500');
INSERT INTO `dt_area` VALUES ('411528', '息县', 'Xixian', '411500');
INSERT INTO `dt_area` VALUES ('411600', '周口市', 'Zhoukou', '410000');
INSERT INTO `dt_area` VALUES ('411602', '川汇区', 'Chuanhui', '411600');
INSERT INTO `dt_area` VALUES ('411621', '扶沟县', 'Fugou', '411600');
INSERT INTO `dt_area` VALUES ('411622', '西华县', 'Xihua', '411600');
INSERT INTO `dt_area` VALUES ('411623', '商水县', 'Shangshui', '411600');
INSERT INTO `dt_area` VALUES ('411624', '沈丘县', 'Shenqiu', '411600');
INSERT INTO `dt_area` VALUES ('411625', '郸城县', 'Dancheng', '411600');
INSERT INTO `dt_area` VALUES ('411626', '淮阳县', 'Huaiyang', '411600');
INSERT INTO `dt_area` VALUES ('411627', '太康县', 'Taikang', '411600');
INSERT INTO `dt_area` VALUES ('411628', '鹿邑县', 'Luyi', '411600');
INSERT INTO `dt_area` VALUES ('411681', '项城市', 'Xiangcheng', '411600');
INSERT INTO `dt_area` VALUES ('411700', '驻马店市', 'Zhumadian', '410000');
INSERT INTO `dt_area` VALUES ('411702', '驿城区', 'Yicheng', '411700');
INSERT INTO `dt_area` VALUES ('411721', '西平县', 'Xiping', '411700');
INSERT INTO `dt_area` VALUES ('411722', '上蔡县', 'Shangcai', '411700');
INSERT INTO `dt_area` VALUES ('411723', '平舆县', 'Pingyu', '411700');
INSERT INTO `dt_area` VALUES ('411724', '正阳县', 'Zhengyang', '411700');
INSERT INTO `dt_area` VALUES ('411725', '确山县', 'Queshan', '411700');
INSERT INTO `dt_area` VALUES ('411726', '泌阳县', 'Biyang', '411700');
INSERT INTO `dt_area` VALUES ('411727', '汝南县', 'Runan', '411700');
INSERT INTO `dt_area` VALUES ('411728', '遂平县', 'Suiping', '411700');
INSERT INTO `dt_area` VALUES ('411729', '新蔡县', 'Xincai', '411700');
INSERT INTO `dt_area` VALUES ('419000', '直辖县级', '', '410000');
INSERT INTO `dt_area` VALUES ('419001', '济源市', 'Jiyuan', '419000');

/*!40000 ALTER TABLE `dt_area` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hibernate_sequence
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;

INSERT INTO `hibernate_sequence` (`next_val`)
VALUES
	(2);

/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` bigint(20) NOT NULL,
  `class1id` varchar(255) DEFAULT NULL,
  `class2id` varchar(255) DEFAULT NULL,
  `fu_dao1id` varchar(255) DEFAULT NULL,
  `fu_dao1name` varchar(255) DEFAULT NULL,
  `fu_dao2id` varchar(255) DEFAULT NULL,
  `fu_dao2name` varchar(255) DEFAULT NULL,
  `group_type` int(11) DEFAULT NULL,
  `img1` varchar(255) DEFAULT NULL,
  `img2` varchar(255) DEFAULT NULL,
  `img3` varchar(255) DEFAULT NULL,
  `member1name` varchar(255) DEFAULT NULL,
  `member2name` varchar(255) DEFAULT NULL,
  `member3name` varchar(255) DEFAULT NULL,
  `number1id` varchar(255) DEFAULT NULL,
  `number1sex` varchar(255) DEFAULT NULL,
  `number1size` varchar(255) DEFAULT NULL,
  `number2id` varchar(255) DEFAULT NULL,
  `number2sex` varchar(255) DEFAULT NULL,
  `number2size` varchar(255) DEFAULT NULL,
  `number3id` varchar(255) DEFAULT NULL,
  `number3sex` varchar(255) DEFAULT NULL,
  `number3size` varchar(255) DEFAULT NULL,
  `special` bit(1) DEFAULT NULL,
  `team_name` varchar(255) DEFAULT NULL,
  `word` bit(1) DEFAULT NULL,
  `word_img` varchar(255) DEFAULT NULL,
  `member1id` varchar(255) DEFAULT NULL,
  `member1sex` varchar(255) DEFAULT NULL,
  `member1size` varchar(255) DEFAULT NULL,
  `member2id` varchar(255) DEFAULT NULL,
  `member2sex` varchar(255) DEFAULT NULL,
  `member2size` varchar(255) DEFAULT NULL,
  `member3id` varchar(255) DEFAULT NULL,
  `member3sex` varchar(255) DEFAULT NULL,
  `member3size` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table project_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_class`;

CREATE TABLE `project_class` (
  `id` bigint(20) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `special` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `project_class` WRITE;
/*!40000 ALTER TABLE `project_class` DISABLE KEYS */;

INSERT INTO `project_class` (`id`, `level`, `name`, `parent_id`, `special`)
VALUES
	(1,1,'机器人大类',0,1),
	(2,2,'机器人小类(A类)',1,0),
	(3,2,'机器人小类(B类)',1,0),
	(4,2,'机器人小类(C类)',1,0);

/*!40000 ALTER TABLE `project_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table school
# ------------------------------------------------------------

DROP TABLE IF EXISTS `school`;

CREATE TABLE `school` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `area_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `passwd` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
